cmake_minimum_required(VERSION 3.4.3)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Platform)

project(sci)

enable_language(CXX)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 0)

set(EXTERN_DIR ${CMAKE_CURRENT_SOURCE_DIR}/extern)
set(TINYXML2_ROOT_DIR ${EXTERN_DIR}/tinyxml2)
set(TESTUTILS_ROOT_DIR ${EXTERN_DIR}/testutils)

set(GOOGLETEST_ROOT_DIR ${EXTERN_DIR}/gmock-1.7.0/gtest)
add_library(googletest
    ${GOOGLETEST_ROOT_DIR}/src/gtest-all.cc
    ${GOOGLETEST_ROOT_DIR}/src/gtest_main.cc
    )
target_compile_features(googletest PUBLIC cxx_deleted_functions)
target_include_directories(googletest PUBLIC
    ${GOOGLETEST_ROOT_DIR}
    ${GOOGLETEST_ROOT_DIR}/include
    )

set(TEST_MAIN ${CMAKE_CURRENT_SOURCE_DIR}/tests/main.cpp)
enable_testing()

add_subdirectory(src)
