/*! \internal \file
 * \brief
 * Tests \Sci-specific test assertions.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_testutils
 */
#include "../testasserts.h"

#include <gtest/gtest.h>

namespace
{

using ::testing::internal::FloatingPoint;

/*! \brief
 * Helper to produce floating-point numbers with specified ULP difference.
 *
 * This doesn't work if the value would change sign.
 *
 * \ingroup module_testutils
 */
template <typename FloatType>
FloatType addUlps(FloatType value, int ulps)
{
    return FloatingPoint<FloatType>::ReinterpretBits(
            FloatingPoint<FloatType>(value).bits() + ulps);
}

using ::sci::test::FloatingPointDifference;

TEST(FloatingPointDifferenceTest, HandlesEqualValues)
{
    FloatingPointDifference diff(1.2, 1.2);
    EXPECT_TRUE(diff.isDouble());
    EXPECT_FALSE(diff.isNaN());
    EXPECT_EQ(0.0, diff.asAbsolute());
    EXPECT_EQ(0U,  diff.asUlps());
    EXPECT_FALSE(diff.signsDiffer());
}

// TODO: Use typed tests to run all the tests for single and double.
TEST(FloatingPointDifferenceTest, HandlesFloatValues)
{
    FloatingPointDifference diff(1.2f, 1.2f);
    EXPECT_FALSE(diff.isDouble());
    EXPECT_FALSE(diff.isNaN());
    EXPECT_EQ(0.0, diff.asAbsolute());
    EXPECT_EQ(0U,  diff.asUlps());
    EXPECT_FALSE(diff.signsDiffer());
}

TEST(FloatingPointDifferenceTest, HandlesZerosOfDifferentSign)
{
    FloatingPointDifference diff(0.0, SCI_DOUBLE_NEGZERO);
    EXPECT_FALSE(diff.isNaN());
    EXPECT_EQ(0.0, diff.asAbsolute());
    EXPECT_EQ(0U,  diff.asUlps());
    EXPECT_TRUE(diff.signsDiffer());
}

TEST(FloatingPointDifferenceTest, HandlesSignComparisonWithZero)
{
    {
        FloatingPointDifference diff(0.0, -1.2);
        EXPECT_FALSE(diff.isNaN());
        EXPECT_DOUBLE_EQ(1.2, diff.asAbsolute());
        EXPECT_TRUE(diff.signsDiffer());
    }
    {
        FloatingPointDifference diff(SCI_DOUBLE_NEGZERO, -1.2);
        EXPECT_FALSE(diff.isNaN());
        EXPECT_DOUBLE_EQ(1.2, diff.asAbsolute());
        EXPECT_FALSE(diff.signsDiffer());
    }
}

TEST(FloatingPointDifferenceTest, HandlesUlpDifferences)
{
    const double first  = 1.0;
    const double second = addUlps(first, 2);
    {
        FloatingPointDifference diff(first, second);
        EXPECT_FALSE(diff.isNaN());
        EXPECT_DOUBLE_EQ(second - first, diff.asAbsolute());
        EXPECT_EQ(2U, diff.asUlps());
        EXPECT_FALSE(diff.signsDiffer());
    }
    {
        FloatingPointDifference diff(second, first);
        EXPECT_FALSE(diff.isNaN());
        EXPECT_DOUBLE_EQ(second - first, diff.asAbsolute());
        EXPECT_EQ(2U, diff.asUlps());
        EXPECT_FALSE(diff.signsDiffer());
    }
}

TEST(FloatingPointDifferenceTest, HandlesUlpDifferenceAcrossZero)
{
    const double            first  = addUlps(SCI_DOUBLE_NEGZERO, 2);
    const double            second = addUlps( 0.0, 2);
    FloatingPointDifference diff(first, second);
    EXPECT_FALSE(diff.isNaN());
    EXPECT_DOUBLE_EQ(second - first, diff.asAbsolute());
    EXPECT_EQ(4U, diff.asUlps());
    EXPECT_TRUE(diff.signsDiffer());
}

TEST(FloatingPointDifferenceTest, HandlesNaN)
{
    const double            first  = std::numeric_limits<double>::quiet_NaN();
    const double            second = 2.0;
    FloatingPointDifference diff(first, second);
    EXPECT_TRUE(diff.isNaN());
}

TEST(FloatingPointToleranceTest, UlpTolerance)
{
    using sci::test::ulpTolerance;

    FloatingPointDifference fequal(1.0, 1.0);
    FloatingPointDifference fulp2(1.0f, addUlps(1.0f, 2));
    EXPECT_TRUE(ulpTolerance(0).isWithin(fequal));
    EXPECT_FALSE(ulpTolerance(1).isWithin(fulp2));
    EXPECT_TRUE(ulpTolerance(2).isWithin(fulp2));

    FloatingPointDifference dequal(1.0, 1.0);
    FloatingPointDifference dulp2(1.0,  addUlps(1.0, 2));
    FloatingPointDifference dulp2f(1.0,  static_cast<double>(addUlps(1.0f, 2)));
    EXPECT_TRUE(ulpTolerance(0).isWithin(dequal));
    EXPECT_TRUE(ulpTolerance(2).isWithin(dulp2));
    EXPECT_FALSE(ulpTolerance(2).isWithin(dulp2f));
}

TEST(FloatingPointToleranceTest, RelativeToleranceAsFloatingPoint)
{
    using sci::test::relativeToleranceAsFloatingPoint;

    FloatingPointDifference fequal(1.0f, 1.0f);
    FloatingPointDifference fulp2(1.0f, addUlps(1.0f, 2));
    FloatingPointDifference fdiff(1.0f, 1.011f);
    FloatingPointDifference fsmall(0.1f, 0.111f);
    FloatingPointDifference fsmall2(0.1f, 0.121f);
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(fequal));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-9).isWithin(fequal));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(fulp2));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 1e-9).isWithin(fulp2));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(fdiff));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(fdiff));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(fsmall));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(0.1, 2e-2).isWithin(fsmall));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(fsmall2));

    FloatingPointDifference dequal(1.0, 1.0);
    FloatingPointDifference dulp2f(1.0, static_cast<double>(addUlps(1.0f, 2)));
    FloatingPointDifference ddiff(1.0, 1.011);
    FloatingPointDifference dsmall(0.1, 0.111);
    FloatingPointDifference dsmall2(0.1, 0.121);
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(dequal));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-9).isWithin(dequal));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(dulp2f));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 1e-9).isWithin(dulp2f));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 1e-2).isWithin(ddiff));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(ddiff));
    EXPECT_TRUE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(dsmall));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(0.1, 2e-2).isWithin(dsmall));
    EXPECT_FALSE(relativeToleranceAsFloatingPoint(1.0, 2e-2).isWithin(dsmall2));
}

TEST(FloatingPointToleranceTest, RelativeToleranceAsUlp)
{
    using sci::test::relativeToleranceAsUlp;

    FloatingPointDifference fequal(1.0f, 1.0f);
    FloatingPointDifference fulp4(1.0f, addUlps(1.0f, 4));
    FloatingPointDifference fsmall(0.1f, addUlps(1.0f, 2) - 0.9f);
    FloatingPointDifference fsmall2(0.1f, addUlps(1.0f, 6) - 0.9f);
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 2).isWithin(fequal));
    EXPECT_FALSE(relativeToleranceAsUlp(1.0, 2).isWithin(fulp4));
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 4).isWithin(fulp4));
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 4).isWithin(fsmall));
    EXPECT_FALSE(relativeToleranceAsUlp(0.1, 4).isWithin(fsmall));
    EXPECT_FALSE(relativeToleranceAsUlp(1.0, 4).isWithin(fsmall2));

    FloatingPointDifference dequal(1.0, 1.0);
    FloatingPointDifference dulp4(1.0, addUlps(1.0, 4));
    FloatingPointDifference dulp4f(1.0,  static_cast<double>(addUlps(1.0f, 4)));
    FloatingPointDifference dsmall(0.1, addUlps(1.0, 2) - 0.9);
    FloatingPointDifference dsmall2(0.1, addUlps(1.0, 6) - 0.9);
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 2).isWithin(dequal));
    EXPECT_FALSE(relativeToleranceAsUlp(1.0, 2).isWithin(dulp4));
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 4).isWithin(dulp4));
    EXPECT_FALSE(relativeToleranceAsUlp(1.0, 4).isWithin(dulp4f));
    EXPECT_TRUE(relativeToleranceAsUlp(1.0, 4).isWithin(dsmall));
    EXPECT_FALSE(relativeToleranceAsUlp(0.1, 4).isWithin(dsmall));
    EXPECT_FALSE(relativeToleranceAsUlp(1.0, 4).isWithin(dsmall2));
}

} // namespace
