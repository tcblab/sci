/*! \internal \file
 * \brief Tests for uniform real distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../uniformrealdistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(UniformRealDistributionTest, GenerateCanonical)
{
    sci::ThreeFry2x64<8>                rng(123456, sci::RandomDomain::Other);

    real (*canonicalGenerator)(sci::ThreeFry2x64<8> &) = sci::generateCanonical<real, std::numeric_limits<real>::digits>;

    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.9902205201172749);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.063721663645969207);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.10528987482419813);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.69516754412279158);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.17578871959656689);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.079381025129182742);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.59553458674603454);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.59252151501626416);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.59742294033136178);
    EXPECT_REAL_EQ(canonicalGenerator(rng), 0.51664365340146001);
}

TEST(UniformRealDistributionTest, Output)
{
    sci::ThreeFry2x64<8>                    rng(123456, sci::RandomDomain::Other);
    sci::UniformRealDistribution<real>    dist(1.0, 10.0);

    EXPECT_REAL_EQ(dist(rng), 9.9119846810554737);
    EXPECT_REAL_EQ(dist(rng), 1.5734949728137229);
    EXPECT_REAL_EQ(dist(rng), 1.9476088734177832);
    EXPECT_REAL_EQ(dist(rng), 7.2565078971051244);
    EXPECT_REAL_EQ(dist(rng), 2.5820984763691017);
    EXPECT_REAL_EQ(dist(rng), 1.7144292261626446);
    EXPECT_REAL_EQ(dist(rng), 6.359811280714311);
    EXPECT_REAL_EQ(dist(rng), 6.3326936351463772);
    EXPECT_REAL_EQ(dist(rng), 6.3768064629822563);
    EXPECT_REAL_EQ(dist(rng), 5.6497928806131403);
}

TEST(UniformRealDistributionTest, Logical)
{
    sci::ThreeFry2x64<8>                rng(123456, sci::RandomDomain::Other);
    sci::UniformRealDistribution<real>  distA(2.0, 5.0);
    sci::UniformRealDistribution<real>  distB(2.0, 5.0);
    sci::UniformRealDistribution<real>  distC(3.0, 5.0);
    sci::UniformRealDistribution<real>  distD(2.0, 4.0);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
    EXPECT_NE(distA, distD);
}


TEST(UniformRealDistributionTest, Reset)
{
    sci::ThreeFry2x64<8>                             rng(123456, sci::RandomDomain::Other);
    sci::UniformRealDistribution<real>               distA(2.0, 5.0);
    sci::UniformRealDistribution<real>               distB(2.0, 5.0);
    sci::UniformRealDistribution<real>::result_type  valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    // Use floating-point test with no tolerance rather than exact
    // test, since the later fails with 32-bit gcc-4.8.4
    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

TEST(UniformRealDistributionTest, AltParam)
{
    sci::ThreeFry2x64<8>                            rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<8>                            rngB(123456, sci::RandomDomain::Other);
    sci::UniformRealDistribution<real>              distA(2.0, 5.0);
    sci::UniformRealDistribution<real>              distB; // default parameters
    sci::UniformRealDistribution<real>::param_type  paramA(2.0, 5.0);
    sci::UniformRealDistribution<real>::result_type valA, valB;

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();

    valA = distA(rngA);
    valB = distB(rngB, paramA);

    // Use floating-point test with no tolerance rather than exact test,
    // since the latter fails with 32-bit gcc-4.8.4
    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

}      // namespace anonymous

}      // namespace sci
