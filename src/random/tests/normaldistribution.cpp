/*! \internal \file
 * \brief Tests for normal distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../normaldistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(NormalDistributionTest, Output)
{
    sci::ThreeFry2x64<8>                rng(123456, sci::RandomDomain::Other);
    sci::NormalDistribution<real>       dist(2.0, 5.0);

    EXPECT_REAL_EQ(dist(rng), -1.1957091691864985);
    EXPECT_REAL_EQ(dist(rng), 3.5801436813991621);
    EXPECT_REAL_EQ(dist(rng), 10.266616394425856);
    EXPECT_REAL_EQ(dist(rng), 10.005895026309037);
    EXPECT_REAL_EQ(dist(rng), 14.550652533167714);
    EXPECT_REAL_EQ(dist(rng), 4.1441429504561507);
    EXPECT_REAL_EQ(dist(rng), -2.3808567945947017);
    EXPECT_REAL_EQ(dist(rng), 2.079897489233129);
    EXPECT_REAL_EQ(dist(rng), 2.5282003135332376);
    EXPECT_REAL_EQ(dist(rng), 12.317670822361183);
}


TEST(NormalDistributionTest, Logical)
{
    sci::ThreeFry2x64<8>           rng(123456, sci::RandomDomain::Other);
    sci::NormalDistribution<real>  distA(2.0, 5.0);
    sci::NormalDistribution<real>  distB(2.0, 5.0);
    sci::NormalDistribution<real>  distC(3.0, 5.0);
    sci::NormalDistribution<real>  distD(2.0, 4.0);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
    EXPECT_NE(distA, distD);
}


TEST(NormalDistributionTest, Reset)
{
    sci::ThreeFry2x64<8>                                rng(123456, sci::RandomDomain::Other);
    sci::NormalDistribution<real>                       distA(2.0, 5.0);
    sci::NormalDistribution<real>                       distB(2.0, 5.0);
    sci::NormalDistribution<real>::result_type          valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    // Use floating-point test with no tolerance rather than
    // exact test, since the latter leads to failures with
    // 32-bit gcc-4.8.4
    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

TEST(NormalDistributionTest, AltParam)
{
    sci::ThreeFry2x64<8>                       rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<8>                       rngB(123456, sci::RandomDomain::Other);
    sci::NormalDistribution<real>              distA(2.0, 5.0);
    sci::NormalDistribution<real>              distB; // default parameters
    sci::NormalDistribution<real>::param_type  paramA(2.0, 5.0);
    sci::NormalDistribution<real>::result_type valA, valB;

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();

    valA = distA(rngA);
    valB = distB(rngB, paramA);

    // Use floating-point test with no tolerance rather than
    // exact test, since the latter leads to failures with
    // 32-bit gcc-4.8.4
    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

}      // namespace anonymous

}      // namespace sci
