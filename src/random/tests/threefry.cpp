/*! \internal \file
 * \brief Tests for the ThreeFry random engine
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../threefry.h"

#include <gtest/gtest.h>

//#include "utility/exceptions.h"

namespace sci
{

namespace
{

class ThreeFry2x64Test : public ::testing::TestWithParam<std::vector<sci_uint64_t> >
{
};
/*
TEST_P(ThreeFry2x64Test, Default)
{
    sci::test::TestReferenceData       data;
    sci::test::TestReferenceChecker    checker(data.rootChecker());
    const std::vector<sci_uint64_t>    input = GetParam();
    std::vector<sci_uint64_t>          result;

    sci::ThreeFry2x64<0>               rng(input[2], input[3]);
    rng.restart(input[0], input[1]);

    result.push_back(rng());
    result.push_back(rng());

    checker.checkSequence(result.begin(), result.end(), "ThreeFry2x64");
}

TEST_P(ThreeFry2x64Test, Fast)
{
    sci::test::TestReferenceData       data;
    sci::test::TestReferenceChecker    checker(data.rootChecker());
    const std::vector<sci_uint64_t>    input = GetParam();
    std::vector<sci_uint64_t>          result;

    sci::ThreeFry2x64Fast<0>           rng(input[2], input[3]);
    rng.restart(input[0], input[1]);

    result.push_back(rng());
    result.push_back(rng());

    checker.checkSequence(result.begin(), result.end(), "ThreeFry2x64Fast");
}

TEST_P(ThreeFry2x64Test, Using40Rounds)
{
    sci::test::TestReferenceData       data;
    sci::test::TestReferenceChecker    checker(data.rootChecker());
    const std::vector<sci_uint64_t>    input = GetParam();
    std::vector<sci_uint64_t>          result;

    sci::ThreeFry2x64General<40, 0>    rng(input[2], input[3]);
    rng.restart(input[0], input[1]);

    result.push_back(rng());
    result.push_back(rng());

    checker.checkSequence(result.begin(), result.end(), "ThreeFry2x64Using40Rounds");
}
*/

/*! \brief Constant array of integers with all bits zeroed.
 *
 *  Reference key and counter input data for known answers test.
 *  The 2x64 flavors of ThreeFry64 will use the first four values, while
 *  the 4x64 version uses all eight.
 */
const std::vector<sci_uint64_t> bitsZero {{
                                              0, 0, 0, 0
                                          }};


/*! \brief Constant array of integers with all bits set to one.
 *
 *  Reference key and counter input data for known answers test.
 *  The 2x64 flavors of ThreeFry64 will use the first four values, while
 *  the 4x64 version uses all eight.
 */
const std::vector<sci_uint64_t> bitsOne {{
                                             0xFFFFFFFFFFFFFFFFULL, 0xFFFFFFFFFFFFFFFFULL,
                                             0xFFFFFFFFFFFFFFFFULL, 0xFFFFFFFFFFFFFFFFULL
                                         }};

/*! \brief Constant array of integers with bitpattern from Pi.
 *
 *  Reference key and counter input data for known answers test.
 *  The 2x64 flavors of ThreeFry64 will use the first four values, while
 *  the 4x64 version uses all eight.
 */
const std::vector<sci_uint64_t> bitsPi {{
                                            0x243f6a8885a308d3ULL, 0x13198a2e03707344ULL,
                                            0xa4093822299f31d0ULL, 0x082efa98ec4e6c89ULL
                                        }};

// Test the known ansers for the ThreeFry random function when the argument
// is (1) all zero, (2) all ones, (3) the bits of pi, for a bunch of different flavors of ThreeFry.
/*
INSTANTIATE_TEST_CASE_P(KnownAnswersTest, ThreeFry2x64Test,
                            ::testing::Values(bitsZero, bitsOne, bitsPi));
*/

// ThreeFry2x64 tests
TEST_F(ThreeFry2x64Test, Logical)
{
    sci::ThreeFry2x64<10> rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<10> rngB(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<10> rngC(123456, sci::RandomDomain::Other);

    rngB();              // draw just once first, so block is the same, but index has changed
    EXPECT_NE(rngA, rngB);
    rngC(); rngC();      // two draws: next block, but index is the same
    EXPECT_NE(rngA, rngC);
    rngA();
    EXPECT_EQ(rngA, rngB);
    rngA();
    EXPECT_EQ(rngA, rngC);
}

TEST_F(ThreeFry2x64Test, InternalCounterSequence)
{
    // 66 bits of internal counter means the first four increments (giving 2*4=8 results)
    // correspond to incrementing word 0, and then we should carry over to word 1.
    sci::ThreeFry2x64<66>        rngA(123456, sci::RandomDomain::Other);
    EXPECT_EQ(rngA(), sci_uint64_t(486462825850895741UL));
    EXPECT_EQ(rngA(), sci_uint64_t(17446101562835174430UL));
    EXPECT_EQ(rngA(), sci_uint64_t(9373088801415375509UL));
    EXPECT_EQ(rngA(), sci_uint64_t(12510376360269611509UL));
    EXPECT_EQ(rngA(), sci_uint64_t(7841771435770901348UL));
    EXPECT_EQ(rngA(), sci_uint64_t(9490443134679953691UL));
    EXPECT_EQ(rngA(), sci_uint64_t(17313062094864687017UL));
    EXPECT_EQ(rngA(), sci_uint64_t(10839367803978367078UL));
    EXPECT_EQ(rngA(), sci_uint64_t(7940925158368031371UL));
    EXPECT_EQ(rngA(), sci_uint64_t(16726189572081543100UL));
    EXPECT_EQ(rngA(), sci_uint64_t(12308886200103827324UL));
    EXPECT_EQ(rngA(), sci_uint64_t(10501766140156377024UL));
    EXPECT_EQ(rngA(), sci_uint64_t(8166398556381502531UL));
    EXPECT_EQ(rngA(), sci_uint64_t(3204143707537302884UL));
    EXPECT_EQ(rngA(), sci_uint64_t(16891264436844784832UL));
    EXPECT_EQ(rngA(), sci_uint64_t(13660213278178626553UL));

    // Make sure nothing goes wrong with the internal counter sequence when we use a full 64-bit word
    sci::ThreeFry2x64<64>        rngB(123456, sci::RandomDomain::Other);
    for (int i = 0; i < 16; i++)
    {
        rngB();
    }

    // Use every single bit for the internal counter
    sci::ThreeFry2x64<128>        rngC(123456, sci::RandomDomain::Other);
    for (int i = 0; i < 16; i++)
    {
        rngC();
    }
}
/*
TEST_F(ThreeFry2x64Test, Reseed)
{
    sci::ThreeFry2x64<10> rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<10> rngB;

    EXPECT_NE(rngA, rngB);
    rngB.seed(123456, sci::RandomDomain::Other);
    EXPECT_EQ(rngA, rngB);
    rngB();                                      // internal counter increments
    rngB.seed(123456, sci::RandomDomain::Other); // reseeding should reset random stream too
    EXPECT_EQ(rngA, rngB);
}

TEST_F(ThreeFry2x64Test, Discard)
{
    sci::ThreeFry2x64<10> rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<10> rngB(123456, sci::RandomDomain::Other);

    for (int i = 0; i < 9; i++)
    {
        rngA();
    }
    rngB.discard(9);
    EXPECT_EQ(rngA, rngB);
}


TEST_F(ThreeFry2x64Test, InvalidCounter)
{
    sci::ThreeFry2x64<10> rngA(123456, sci::RandomDomain::Other);

    // Highest 10 bits of counter reserved for the internal counter.
    EXPECT_THROW_SCI(rngA.restart(0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF), sci::InternalError);
}

TEST_F(ThreeFry2x64Test, ExhaustInternalCounter)
{
    sci::ThreeFry2x64<2> rngA(123456, sci::RandomDomain::Other);

    // 2 bits for internal counter and 2 64-results per counter means 8 results are fine
    for (int i = 0; i < 8; i++)
    {
        rngA();
    }
    // ... but the 9th time we have exhausted the internal counter space.
    EXPECT_THROW_SCI(rngA(), sci::InternalError);
}
*/
}      // namespace anonymous

}      // namespace sci
