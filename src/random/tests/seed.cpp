/*! \internal \file
 * \brief Tests for random seed functions
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../seed.h"

#include <gtest/gtest.h>

namespace sci
{

namespace
{

// Test the random device call
TEST(SeedTest, makeRandomSeed)
{
    // Unlike Sony, we do not use "4" as a constant random value, so the only
    // thing we can check for the random device is that multiple calls to
    // it produce different results.
    // We choose to ignore the 2^-64 probability this will happen by chance;
    // if you execute the unit tests once per second you might have to run them
    // an extra time rougly once per 300 billion years - apologies in advance!

    sci_uint64_t i0 = makeRandomSeed();
    sci_uint64_t i1 = makeRandomSeed();

    EXPECT_NE(i0, i1);
}

}      // namespace anonymous

}      // namespace sci
