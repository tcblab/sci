/*! \internal \file
 * \brief Tests for uniform integer distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../uniformintdistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(UniformIntDistributionTest, Output)
{
    sci::ThreeFry2x64<8>                    rng(123456, sci::RandomDomain::Other);
    sci::UniformIntDistribution<int>        dist(1, 1000);

    EXPECT_EQ(dist(rng), 522);
    EXPECT_EQ(dist(rng), 489);
    EXPECT_EQ(dist(rng), 950);
    EXPECT_EQ(dist(rng), 567);
    EXPECT_EQ(dist(rng), 792);
    EXPECT_EQ(dist(rng), 864);
    EXPECT_EQ(dist(rng), 35);
    EXPECT_EQ(dist(rng), 605);
    EXPECT_EQ(dist(rng), 640);
    EXPECT_EQ(dist(rng), 118);
}


TEST(UniformIntDistributionTest, Logical)
{
    sci::ThreeFry2x64<8>               rng(123456, sci::RandomDomain::Other);
    sci::UniformIntDistribution<int>   distA(2, 5);
    sci::UniformIntDistribution<int>   distB(2, 5);
    sci::UniformIntDistribution<int>   distC(3, 5);
    sci::UniformIntDistribution<int>   distD(2, 4);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
    EXPECT_NE(distA, distD);
}


TEST(UniformIntDistributionTest, Reset)
{
    sci::ThreeFry2x64<8>                         rng(123456, sci::RandomDomain::Other);
    sci::UniformIntDistribution<int>             distA(2, 5);
    sci::UniformIntDistribution<int>             distB(2, 5);
    sci::UniformIntDistribution<>::result_type   valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    EXPECT_EQ(valA, valB);
}

TEST(UniformIntDistributionTest, AltParam)
{
    sci::ThreeFry2x64<8>                          rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<8>                          rngB(123456, sci::RandomDomain::Other);
    sci::UniformIntDistribution<int>              distA(2, 5);
    sci::UniformIntDistribution<int>              distB; // default parameters
    sci::UniformIntDistribution<int>::param_type  paramA(2, 5);

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();
    EXPECT_EQ(distA(rngA), distB(rngB, paramA));
}


}      // namespace anonymous

}      // namespace sci
