/*! \internal \file
 * \brief Tests for tabulated normal distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../tabulatednormaldistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(TabulatedNormalDistributionTest, Output14)
{
    sci::ThreeFry2x64<2>                 rng(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<>   dist(2.0, 5.0); // Use default 14-bit resolution

    EXPECT_FLOAT_EQ(dist(rng), 6.2040744);
    EXPECT_FLOAT_EQ(dist(rng), -5.0443349);
    EXPECT_FLOAT_EQ(dist(rng), -3.8484411);
    EXPECT_FLOAT_EQ(dist(rng), -3.5752203);
    EXPECT_FLOAT_EQ(dist(rng), 1.8067989);
    EXPECT_FLOAT_EQ(dist(rng), 6.2412276);
    EXPECT_FLOAT_EQ(dist(rng), -1.5439517);
    EXPECT_FLOAT_EQ(dist(rng), -8.951438);
    EXPECT_FLOAT_EQ(dist(rng), -2.3151355);
    EXPECT_FLOAT_EQ(dist(rng), 2.7598448);
}

TEST(TabulatedNormalDistributionTest, Output16)
{
    sci::ThreeFry2x64<2>                          rng(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<float, 16>   dist(2.0, 5.0); // Use larger 16-bit table

    EXPECT_FLOAT_EQ(dist(rng), 1.37098);
    EXPECT_FLOAT_EQ(dist(rng), 9.8791161);
    EXPECT_FLOAT_EQ(dist(rng), 8.1999226);
    EXPECT_FLOAT_EQ(dist(rng), -4.692863);
    EXPECT_FLOAT_EQ(dist(rng), 0.35591698);
    EXPECT_FLOAT_EQ(dist(rng), 13.225243);
    EXPECT_FLOAT_EQ(dist(rng), 3.8506312);
    EXPECT_FLOAT_EQ(dist(rng), -17.372868);
    EXPECT_FLOAT_EQ(dist(rng), -0.6433773);
    EXPECT_FLOAT_EQ(dist(rng), -1.8184407);
}

TEST(TabulatedNormalDistributionTest, OutputDouble14)
{
    sci::ThreeFry2x64<2>                          rng(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<double>      dist(2.0, 5.0);

    EXPECT_DOUBLE_EQ(dist(rng), 6.2040743073882094);
    EXPECT_DOUBLE_EQ(dist(rng), -5.0443347858799923);
    EXPECT_DOUBLE_EQ(dist(rng), -3.8484410453534643);
    EXPECT_DOUBLE_EQ(dist(rng), -3.5752202809260982);
    EXPECT_DOUBLE_EQ(dist(rng), 1.8067989676891278);
    EXPECT_DOUBLE_EQ(dist(rng), 6.2412274362065912);
    EXPECT_DOUBLE_EQ(dist(rng), -1.5439517275812444);
    EXPECT_DOUBLE_EQ(dist(rng), -8.9514357643656126);
    EXPECT_DOUBLE_EQ(dist(rng), -2.315135578014341);
    EXPECT_DOUBLE_EQ(dist(rng), 2.7598447480822306);
}

TEST(TabulatedNormalDistributionTest, Logical)
{
    sci::ThreeFry2x64<2>                 rng(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<>   distA(2.0, 5.0);
    sci::TabulatedNormalDistribution<>   distB(2.0, 5.0);
    sci::TabulatedNormalDistribution<>   distC(3.0, 5.0);
    sci::TabulatedNormalDistribution<>   distD(2.0, 4.0);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
    EXPECT_NE(distA, distD);
}


TEST(TabulatedNormalDistributionTest, Reset)
{
    sci::ThreeFry2x64<2>                                      rng(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<>                        distA(2.0, 5.0);
    sci::TabulatedNormalDistribution<>                        distB(2.0, 5.0);
    sci::TabulatedNormalDistribution<>::result_type           valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

TEST(TabulatedNormalDistributionTest, AltParam)
{
    sci::ThreeFry2x64<2>                            rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<2>                            rngB(123456, sci::RandomDomain::Other);
    sci::TabulatedNormalDistribution<>              distA(2.0, 5.0);
    sci::TabulatedNormalDistribution<>              distB;
    sci::TabulatedNormalDistribution<>::param_type  paramA(2.0, 5.0);

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();
    EXPECT_REAL_EQ_TOL(distA(rngA), distB(rngB, paramA), sci::test::ulpTolerance(0));
}

TEST(TabulatedNormalDistributionTableTest, HasValidProperties)
{
    std::vector<real> table = TabulatedNormalDistribution<real>::makeTable();

    EXPECT_EQ(table.size() % 2, 0) << "Table must have even number of entries";

    size_t halfSize     = table.size() / 2;
    double sumOfSquares = 0.0;
    // accept errors of a few ULP since the exact value of the summation
    // below will depend on whether the compiler issues FMA instructions
    auto   tolerance    = sci::test::ulpTolerance(10);
    for (size_t i = 0, iFromEnd = table.size()-1; i < halfSize; ++i, --iFromEnd)
    {
        EXPECT_REAL_EQ_TOL(table.at(i), -table.at(iFromEnd), tolerance)
        << "Table is not an odd-valued function for entries " << i << " and " << iFromEnd;
        // Add up the squares of the table values in order of ascending
        // magnitude (to minimize accumulation of round-off error).
        sumOfSquares += table.at(i) * table.at(i) + table.at(iFromEnd) * table.at(iFromEnd);
    }

    double variance = sumOfSquares / table.size();
    EXPECT_REAL_EQ_TOL(1.0, variance, tolerance) << "Table should have unit variance";
}

}      // namespace anonymous

}      // namespace sci
