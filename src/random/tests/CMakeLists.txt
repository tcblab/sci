set(SOURCES
    exponentialdistribution.cpp
    gammadistribution.cpp
    normaldistribution.cpp
    seed.cpp
    tabulatednormaldistribution.cpp
    threefry.cpp
    uniformintdistribution.cpp
    uniformrealdistribution.cpp
    )

add_library(${TEST_OBJLIB} OBJECT ${SOURCES})

target_include_directories(${TEST_OBJLIB} PUBLIC
    ${SCIDIR}
    ${GOOGLETEST_ROOT_DIR}/include
        )
