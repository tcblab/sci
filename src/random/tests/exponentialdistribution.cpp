/*! \internal \file
 * \brief Tests for exponential distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../exponentialdistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(ExponentialDistributionTest, Output)
{
    sci::ThreeFry2x64<8>                rng(123456, sci::RandomDomain::Other);
    sci::ExponentialDistribution<real>  dist(5.0);

    // The implementation of the exponential distribution both in all current C++
    // standard libraries tested is fragile since it computes an intermediate value by subtracting
    // a random number in [0,1) from 1.0. This should not affect the accuracy of the final distribution,
    // but depending on the compiler optimization individual values will show a somewhat larger
    // fluctuation compared to the other distributions.
    auto tolerance = sci::test::relativeToleranceAsFloatingPoint(1.0, 1e-6);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.92549379561501188, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.013168495762002016, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.022251099130735842, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.23759859566034716, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.038665674736610124, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.016541807278810619, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.18103781021478368, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.17955342905619712, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.18197374957106757, tolerance);
    EXPECT_DOUBLE_EQ_TOL(dist(rng), 0.14540022394339377, tolerance);

}


TEST(ExponentialDistributionTest, Logical)
{
    sci::ThreeFry2x64<8>                rng(123456, sci::RandomDomain::Other);
    sci::ExponentialDistribution<real>  distA(2.0);
    sci::ExponentialDistribution<real>  distB(2.0);
    sci::ExponentialDistribution<real>  distC(3.0);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
}


TEST(ExponentialDistributionTest, Reset)
{
    sci::ThreeFry2x64<8>                                rng(123456, sci::RandomDomain::Other);
    sci::ExponentialDistribution<real>                  distA(2.0);
    sci::ExponentialDistribution<real>                  distB(2.0);
    sci::ExponentialDistribution<>::result_type         valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

TEST(ExponentialDistributionTest, AltParam)
{
    sci::ThreeFry2x64<8>                            rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<8>                            rngB(123456, sci::RandomDomain::Other);
    sci::ExponentialDistribution<real>              distA(2.0);
    sci::ExponentialDistribution<real>              distB; // default parameters
    sci::ExponentialDistribution<real>::param_type  paramA(2.0);

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();
    EXPECT_REAL_EQ_TOL(distA(rngA), distB(rngB, paramA), sci::test::ulpTolerance(0));
}

}      // namespace anonymous

}      // namespace sci
