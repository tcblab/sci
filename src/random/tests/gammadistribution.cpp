/*! \internal \file
 * \brief Tests for gamma distribution
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_random
 */
#include "../gammadistribution.h"

#include <gtest/gtest.h>

#include "../threefry.h"

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(GammaDistributionTest, Output)
{
    sci::ThreeFry2x64<8>               rng(123456, sci::RandomDomain::Other);
    sci::GammaDistribution<real>       dist(2.0, 5.0);

    EXPECT_REAL_EQ(dist(rng), 7.2300576785074835);
    EXPECT_REAL_EQ(dist(rng), 7.2758594071167826);
    EXPECT_REAL_EQ(dist(rng), 5.2014783979537693);
    EXPECT_REAL_EQ(dist(rng), 17.065501715812729);
    EXPECT_REAL_EQ(dist(rng), 32.066124987428772);
    EXPECT_REAL_EQ(dist(rng), 5.4208824941016109);
    EXPECT_REAL_EQ(dist(rng), 3.9078264858447538);
    EXPECT_REAL_EQ(dist(rng), 3.1450042317477274);
    EXPECT_REAL_EQ(dist(rng), 9.3517746645342825);
    EXPECT_REAL_EQ(dist(rng), 3.2296141721313592);
}

TEST(GammaDistributionTest, Logical)
{
    sci::ThreeFry2x64<8>           rng(123456, sci::RandomDomain::Other);
    sci::GammaDistribution<real>   distA(2.0, 5.0);
    sci::GammaDistribution<real>   distB(2.0, 5.0);
    sci::GammaDistribution<real>   distC(3.0, 5.0);
    sci::GammaDistribution<real>   distD(2.0, 4.0);

    EXPECT_EQ(distA, distB);
    EXPECT_NE(distA, distC);
    EXPECT_NE(distA, distD);
}


TEST(GammaDistributionTest, Reset)
{
    sci::ThreeFry2x64<8>                                rng(123456, sci::RandomDomain::Other);
    sci::GammaDistribution<real>                        distA(2.0, 5.0);
    sci::GammaDistribution<real>                        distB(2.0, 5.0);
    sci::GammaDistribution<>::result_type               valA, valB;

    valA = distA(rng);

    distB(rng);
    rng.restart();
    distB.reset();

    valB = distB(rng);

    EXPECT_REAL_EQ_TOL(valA, valB, sci::test::ulpTolerance(0));
}

TEST(GammaDistributionTest, AltParam)
{
    sci::ThreeFry2x64<8>                      rngA(123456, sci::RandomDomain::Other);
    sci::ThreeFry2x64<8>                      rngB(123456, sci::RandomDomain::Other);
    sci::GammaDistribution<real>              distA(2.0, 5.0);
    sci::GammaDistribution<real>              distB; // default parameters
    sci::GammaDistribution<real>::param_type  paramA(2.0, 5.0);

    EXPECT_NE(distA(rngA), distB(rngB));
    rngA.restart();
    rngB.restart();
    distA.reset();
    distB.reset();
    EXPECT_REAL_EQ_TOL(distA(rngA), distB(rngB, paramA), sci::test::ulpTolerance(0));
}

}      // namespace anonymous

}      // namespace sci
