#include "tabulatednormaldistribution.h"

namespace sci
{

// MSVC does not handle extern template class members correctly even in MSVC 2015,
// so in that case we have to instantiate in every object using it.
#if !defined(_MSC_VER)
// This is by far the most common version of the normal distribution table,
// so we use this as an extern template specialization to avoid instantiating
// the table in all files using it, unless the user has requested a different
// precision or resolution.
template<>
const std::vector<real> TabulatedNormalDistribution<real, c_TabulatedNormalDistributionDefaultBits>::c_table_ = TabulatedNormalDistribution<real, c_TabulatedNormalDistributionDefaultBits>::makeTable();
#else
// Avoid compiler warnings about no public symbols
void TabulatedNormalDistributionDummy(){}
#endif

}
