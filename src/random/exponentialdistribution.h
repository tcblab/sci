/*! \file
 * \brief The exponential distribution
 *
 * Portable version of the exponential distribution that generates the same
 * sequence on all platforms. Since stdlibc++ and libc++ provide different
 * sequences we prefer this one so unit tests produce the same values on all
 * platforms.
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \inpublicapi
 * \ingroup module_random
 */

#ifndef SCI_RANDOM_EXPONENTIALDISTRIBUTION_H
#define SCI_RANDOM_EXPONENTIALDISTRIBUTION_H

#include <cmath>

#include <limits>

#include "uniformrealdistribution.h"
#include "utility/classhelpers.h"

/*
 * The portable version of the exponential distribution (to make sure we get the
 * same values on all platforms) has been modified from the LLVM libcxx headers,
 * distributed under the MIT license:
 *
 * Copyright (c) The LLVM compiler infrastructure
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace sci
{

/*! \brief Exponential distribution
 *
 *  The C++ standard library does provide an exponential distribution, but even
 *  though they all sample from a correct distribution, different standard
 *  library implementations appear to return different sequences of numbers
 *  for the same random number generator. To make it easier to use the
 *  unit tests that depend on random numbers we have our own implementation.
 *
 * \tparam RealType Floating-point type, real by default.
 */
template<class RealType = real>
class ExponentialDistribution
{
    public:
        /*! \brief Type of values returned */
        typedef RealType result_type;

        /*! \brief Exponential distribution parameters */
        class param_type
        {
            /*! \brief The lambda/decay parameter */
            result_type  lambda_;

            public:
                /*! \brief Reference back to the distribution class */
                typedef ExponentialDistribution distribution_type;

                /*! \brief Construct parameter block
                 *
                 * \param lambda   lambda/decay parameter
                 */
                explicit param_type(result_type lambda = 1.0)
                    : lambda_(lambda) {}

                /*! \brief Return lambda parameter */
                result_type lambda() const { return lambda_; }

                /*! \brief True if two parameter sets will return the same exponential distribution.
                 *
                 * \param x  Instance to compare with.
                 */
                bool
                operator==(const param_type &x) const
                {
                    return lambda_ == x.lambda_;
                }

                /*! \brief True if two parameter sets will return different exponential distributions
                 *
                 * \param x  Instance to compare with.
                 */
                bool
                operator!=(const param_type &x) const { return !operator==(x); }
        };

    public:

        /*! \brief Construct new distribution with given floating-point parameter.
         *
         * \param lambda   lambda/decay parameter

         */
        explicit ExponentialDistribution(result_type lambda = 1.0)
            : param_(param_type(lambda)) {}

        /*! \brief Construct new distribution from parameter class
         *
         * \param param  Parameter class as defined inside sci::ExponentialDistribution.
         */
        explicit ExponentialDistribution(const param_type &param) : param_(param) {}

        /*! \brief Flush all internal saved values  */
        void
        reset() {}

        /*! \brief Return values from exponential distribution with internal parameters
         *
         *  \tparam Rng   Random engine class
         *
         *  \param  g     Random engine
         */
        template<class Rng>
        result_type
        operator()(Rng &g) { return (*this)(g, param_); }

        /*! \brief Return value from exponential distribution with given parameters
         *
         *  \tparam Rng   Random engine class
         *
         *  \param  g     Random engine
         *  \param  param Parameters to use
         */
        template<class Rng>
        result_type
        operator()(Rng &g, const param_type &param)
        {
            return -std::log(result_type(1) -
                             generateCanonical<result_type,
                                               std::numeric_limits<result_type>::digits>(g)) / param.lambda();
        }

        /*! \brief Return the lambda parameter of the exponential distribution */
        result_type
        lambda() const { return param_.lambda(); }

        /*! \brief Return the full parameter class of exponential distribution */
        param_type param() const { return param_; }

        /*! \brief Smallest value that can be returned from exponential distribution */
        result_type
        min() const { return 0; }

        /*! \brief Largest value that can be returned from exponential distribution */
        result_type
        max() const { return std::numeric_limits<result_type>::infinity(); }

        /*! \brief True if two exponential distributions will produce the same values.
         *
         * \param  x     Instance to compare with.
         */
        bool
        operator==(const ExponentialDistribution &x) const
        { return param_ == x.param_; }

        /*! \brief True if two exponential distributions will produce different values.
         *
         * \param  x     Instance to compare with.
         */
        bool
        operator!=(const ExponentialDistribution &x) const
        { return !operator==(x); }

    private:
        /*! \brief Internal value for parameters, can be overridden at generation time. */
        param_type param_;

        SCI_DISALLOW_COPY_AND_ASSIGN(ExponentialDistribution);
};

}      // namespace sci

#endif // SCI_MATH_RANDOM_H
