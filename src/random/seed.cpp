#include "seed.h"

#include "utility/basedefinitions.h"

namespace sci
{

sci_uint64_t
makeRandomSeed()
{
    std::random_device  rd;
    sci_uint64_t        result;
    std::size_t         deviceBits = std::numeric_limits<std::random_device::result_type>::digits;
    std::size_t         resultBits = std::numeric_limits<sci_uint64_t>::digits;

    result = static_cast<sci_uint64_t>(rd());

    for (std::size_t bits = deviceBits; bits < resultBits; bits += deviceBits)
    {
        result = (result << deviceBits) | static_cast<sci_uint64_t>(rd());
    }

    return result;
}

}
