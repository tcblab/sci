/*! \file
 * \brief Random seed and domain utilities
 *
 * This file contains utilities to create true random seeds from the system,
 * and logic to keep track of different random domains for random engines such
 * as ThreeFry that can take a second seed value.
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \inpublicapi
 * \ingroup module_random
 */

#ifndef SCI_RANDOM_SEED_H
#define SCI_RANDOM_SEED_H

#include <random>

#include "utility/basedefinitions.h"

namespace sci
{

/*! \brief Return 64 random bits from the random device, suitable as seed.
 *
 *  If the internal random device output is smaller than 64 bits, this routine
 *  will use multiple calls internally until we have 64 bits of random data.
 *
 *  \return 64-bit unsigned integer with random bits.
 */
sci_uint64_t
makeRandomSeed();

/*! \brief Random device
 *
 *  For now this is identical to the standard library, but since we use
 *  this random module for all other random engines and distributions
 *  it is convenient to have this too in the same module.
 */
typedef std::random_device RandomDevice;

/*! \brief Enumerated values for fixed part of random seed (domain)
 *
 *  Random numbers might be used in many places, so to avoid identical
 *  streams the random seeds should be different. Instead of keeping track of
 *  several different user-provided seeds, it is better to use the fact that
 *  generators like ThreeFry take two 64-bit keys, and combine a general
 *  user-provided 64-bit random seed with a second constant value from this list
 *  to make each stream guaranteed unique.
 *
 *  \note There is no reason to go overboard with adding options; we only
 *        need to guarantee different streams for cases that might be present
 *        simultaneously in a single simulation. As an example, two different
 *        integrators (or thermostats) can reuse the same domain.
 *  \note When you do add options, leave some space between the values so
 *        you can group new options with old ones without changing old values.
 */
enum class RandomDomain
{
    Other                    = 0x00000000,   //!< Generic - stream uniqueness is not important
    MaxwellVelocities        = 0x00001000,   //!< Veolcity assignment from Maxwell distribution
    TestParticleInsertion    = 0x00002000,   //!< Test particle insertion
    UpdateCoordinates        = 0x00003000,   //!< Particle integrators
    UpdateConstraints        = 0x00004000,   //!< Second integrator step for constraints
    Thermostat               = 0x00005000,   //!< Stochastic temperature coupling
    Barostat                 = 0x00006000,   //!< Stochastic pressure coupling
    ReplicaExchange          = 0x00007000,   //!< Replica exchange metropolis moves
    ExpandedEnsemble         = 0x00008000    //!< Expanded ensemble lambda moves
};

}      // namespace sci

#endif // SCI_RANDOM_SEED_H
