set(SCIDIR ${CMAKE_CURRENT_SOURCE_DIR})

set(SUBDIRS math random utility testutils)

function(sci_install_headers)
    file(RELATIVE_PATH _dest ${PROJECT_SOURCE_DIR}/src ${CMAKE_CURRENT_LIST_DIR})
    install(
        FILES       ${ARGN}
        DESTINATION "${INCL_INSTALL_DIR}/${_dest}"
        COMPONENT   development)
endfunction()

foreach(subdir ${SUBDIRS})
    add_subdirectory(${subdir})
endforeach()

add_library(sci
    $<TARGET_OBJECTS:math>
    $<TARGET_OBJECTS:random>
    $<TARGET_OBJECTS:utility>
    )
