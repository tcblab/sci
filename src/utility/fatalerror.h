/*! \file
 * \brief
 * Declares fatal error handling and debugging routines for C code.
 *
 * \inpublicapi
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_FATALERROR_H
#define SCI_UTILITY_FATALERROR_H

#include <stdarg.h>
#include <stdio.h>

#include "utility/basedefinitions.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! \brief
 * Debug log file.
 *
 * Functions can write to this file for debug info.
 * Before writing to it, it should be checked whether the file is not NULL:
 * \code
   if (debug)
   {
       fprintf(debug, "%s", "Debug text");
   }
   \endcode
 */
extern FILE *debug;
/** Whether extra debugging is enabled. */
extern bool  sci_debug_at;

/*! \brief
 * Initializes debugging variables.
 *
 * This function is not threadsafe.  It should be called as part of
 * initializing \Gromacs, before any other thread accesses the library.
 * For command line programs, sci::CommandLineModuleManager takes care
 * of this if the user requests debugging.
 */
void sci_init_debug(const int dbglevel, const char *dbgfile);

/** Returns TRUE when the program was started in debug mode */
bool bDebugMode(void);

/** Implementation for where(). */
void
_where(const char *file, int line);
/** Prints filename and line to stdlog. */
#define where() _where(__FILE__, __LINE__)

/** Sets the log file for printing error messages. */
void
sci_fatal_set_log_file(FILE *fp);

/** Function pointer type for fatal error handler callback. */
typedef void (*sci_error_handler_t)(const char *title, const char *msg, const char *file, int line);

/*! \brief
 * Sets an error handler for sci_fatal() and other fatal error routines.
 *
 * The default handler prints the message.
 * \Gromacs will terminate the program after the error handler returns.
 * To make sci_fatal_collective() work, the error handler should not terminate
 * the program, as it cannot know what is the desired way of termination.
 * The message passed to the handler may be a multi-line string.
 *
 * \see sci_fatal()
 */
void sci_set_error_handler(sci_error_handler_t func);

/** Identifies the state of the program on a fatal error. */
enum ExitType
{
    /*! \brief
     * Clean exit is possible.
     *
     * There should be no concurrently executing code that might be accessing
     * global objects, and all MPI ranks should reach the same fatal error.
     */
    ExitType_CleanExit,
    /*! \brief
     * Program needs to be aborted.
     *
     * There are no preconditions for this state.
     */
    ExitType_Abort,
    /*! \brief
     * Program needs to be aborted, but some other rank is responsible of it.
     *
     * There should be some other MPI rank that reaches the same fatal error,
     * but uses ExitType_Abort.  The other ranks can then use
     * ExitType_NonMasterAbort to wait for that one rank to issue the abort.
     */
    ExitType_NonMasterAbort
};

/*! \brief
 * Helper function to terminate the program on a fatal error.
 *
 * \param[in] exitType  Identifies the state of the program at the time of the
 *    call, determining how the program can be terminated.
 * \param[in] returnValue  Exit code for the program, for cases where it can be
 *    used.
 */
sci_noreturn void sci_exit_on_fatal_error(enum ExitType exitType, int returnValue);

/*! \brief
 * Low-level fatal error reporting routine for collective MPI errors.
 *
 * This function works as sci_fatal(), but provides additional control for
 * cases where it is known that the same error occurs on multiple MPI ranks.
 * The error handler is called only if \p bMaster is `TRUE`, and MPI_Finalize()
 * is called instead of MPI_Abort() in MPI-enabled \Gromacs if \p bFinalize is
 * `TRUE`.
 *
 * This is used to implement sci_fatal_collective() (which cannot be declared
 * here, since it would bring with it mdrun-specific dependencies).
 */
sci_noreturn void
sci_fatal_mpi_va(int fatal_errno, const char *file, int line,
                 bool bMaster, bool bFinalize,
                 const char *fmt, va_list ap);

/*! \brief
 * Fatal error reporting routine for \Gromacs.
 *
 * This function prints a fatal error message with a header that contains the
 * source file and line number of the call, followed by the string specified by
 * \p fmt and supplied parameters.
 * If \p fatal_errno is 0, only the message and arguments are printed.
 * If \p fatal_errno is a legal system errno or -1, a perror()-like message is
 * printed after the first message; if fatal_errno is -1, the last system errno
 * will be used.
 * The format of \p fmt uses printf()-like formatting.
 *
 * In case all MPI processes want to stop with the same fatal error,
 * use sci_fatal_collective(), declared in network.h,
 * to avoid having as many error messages as processes.
 *
 * The first three parameters can be provided through ::FARGS:
 * \code
   sci_fatal(FARGS, fmt, ...);
   \endcode
 */
sci_noreturn void
sci_fatal(int fatal_errno, const char *file, int line, const char *fmt, ...);
/** Helper macro to pass first three parameters to sci_fatal(). */
#define FARGS 0, __FILE__, __LINE__

/** Implementation for sci_error(). */
sci_noreturn void _sci_error(const char *key, const char *msg, const char *file, int line);
/*! \brief
 * Alternative fatal error routine with canned messages.
 *
 * This works as sci_fatal(), except that a generic error message is added
 * based on a string key, and printf-style formatting is not supported.
 * Should not typically be called directly, but through sci_call() etc.
 */
#define sci_error(key, msg) _sci_error(key, msg, __FILE__, __LINE__)

/*! \name Fatal error routines for certain types of errors
 *
 * These wrap sci_error() and provide the \p key parameter as one of the
 * recognized strings.
 */
/*! \{ */
#define sci_call(msg)   sci_error("call", msg)
#define sci_comm(msg)   sci_error("comm", msg)
#define sci_file(msg)   sci_error("file", msg)
#define sci_impl(msg)   sci_error("impl", msg)
#define sci_incons(msg) sci_error("incons", msg)
#define sci_input(msg)  sci_error("input", msg)
#define sci_mem(msg)    sci_error("mem", msg)
#define sci_open(fn)    sci_error("open", fn)
/*! \} */

/*! \brief
 * Implementation for range_check() and range_check_mesg().
 *
 * \p warn_str can be NULL.
 */
void _range_check(int n, int n_min, int n_max, const char *warn_str,
                  const char *var,
                  const char *file, int line);

/*! \brief
 * Checks that a variable is within a range.
 *
 * If \p n is not in range [n_min, n_max), a fatal error is raised.
 * \p n_min is inclusive, but \p n_max is not.
 */
#define range_check_mesg(n, n_min, n_max, str) _range_check(n, n_min, n_max, str,#n, __FILE__, __LINE__)

/*! \brief
 * Checks that a variable is within a range.
 *
 * This works as range_check_mesg(), but with a default error message.
 */
#define range_check(n, n_min, n_max) _range_check(n, n_min, n_max, NULL,#n, __FILE__, __LINE__)

/*! \brief
 * Prints a warning message to stderr.
 *
 * The format of \p fmt uses printf()-like formatting.
 * The message string should NOT start with "WARNING"
 * and should NOT end with a newline.
 */
void sci_warning(const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif
