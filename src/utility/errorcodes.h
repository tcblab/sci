/*! \file
 * \brief
 * Declares error codes and related functions for fatal error handling.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_ERRORCODES_H
#define SCI_UTILITY_ERRORCODES_H

namespace sci
{

/*! \addtogroup module_utility
 * \{
 */

/*! \brief
 * Possible error return codes from Gromacs functions.
 */
enum ErrorCode
{
    //! Zero for successful return.
    eeOK,

    //! Not enough memory to complete operation.
    eeOutOfMemory,
    //! Provided file could not be opened.
    eeFileNotFound,
    //! System I/O error.
    eeFileIO,
    //! Requested tolerance cannot be achieved.
    eeTolerance,

    /*! \name Error codes for buggy code
     *
     * Error codes below are for internal error checking; if triggered, they
     * should indicate a bug in the code.
     * \{
     */
    //! Requested feature not yet implemented.
    eeNotImplemented,
    //! Input value violates API specification.
    eeInvalidValue,
    //! Invalid routine called or wrong calling sequence detected.
    eeInvalidCall,
    //! Internal consistency check failed.
    eeInternalError,
    //! API specification was violated.
    eeAPIError,
    //! Range consistency check failed.
    eeRange,
    //! Communication consistency check failed.
    eeCommunication,
    //!\}

    //! Unknown error detected.
    eeUnknownError,
};

/*! \brief
 * Returns a short string description of an error code.
 *
 * \param[in] errorcode Error code to retrieve the string for.
 * \returns   A constant string corresponding to \p errorcode.
 *
 * If \p errorcode is not one of those defined for ::sci::ErrorCode,
 * the string corresponding to ::eeUnknownError is returned.
 *
 * This function does not throw.
 */
const char *getErrorCodeString(int errorcode);

/*!\}*/

} // namespace sci

#endif
