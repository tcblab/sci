/*! \file
 * \brief
 * Declares common utility classes and macros.
 *
 * This header contains helpers used to implement classes in the library.
 * It is installed, because the helpers are used in installed headers, but
 * typically users of the library should not need to be aware of these helpers.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \inlibraryapi
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_CLASSHELPERS_H
#define SCI_UTILITY_CLASSHELPERS_H

#include <memory>

namespace sci
{

#ifdef DOXYGEN
/*! \brief
 * Macro to declare a class non-copyable and non-assignable.
 *
 * For consistency, should appear last in the class declaration.
 *
 * \ingroup module_utility
 */
#define SCI_DISALLOW_COPY_AND_ASSIGN(ClassName)
#else
#define SCI_DISALLOW_COPY_AND_ASSIGN(ClassName) \
    ClassName &operator=(const ClassName &) = delete;   \
    ClassName(const ClassName &)            = delete
#endif
/*! \brief
 * Macro to declare a class non-assignable.
 *
 * For consistency, should appear last in the class declaration.
 *
 * \ingroup module_utility
 */
#define SCI_DISALLOW_ASSIGN(ClassName) \
    ClassName &operator=(const ClassName &) = delete

#ifdef DOXYGEN
/*! \brief
 * Macro to declare default constructors
 *
 * Intended for copyable interfaces or bases classes which require to create custom
 * destructor (e.g. protected or virtual) but need the default constructors.
 *
 * \ingroup module_utility
 */
#define SCI_DEFAULT_CONSTRUCTORS(ClassName)
#else
#define SCI_DEFAULT_CONSTRUCTORS(ClassName) \
    ClassName()                                             = default;    \
    ClassName                 &operator=(const ClassName &) = default;    \
    ClassName(const ClassName &)                            = default;    \
    ClassName                 &operator=(ClassName &&)      = default;    \
    ClassName(ClassName &&)                                 = default
#endif

/*! \brief
 * Helper class to manage a pointer to a private implementation class.
 *
 * This helper provides the following benefits (all but the last could also be
 * achieved with std::unique_ptr):
 *  - Automatic memory management: the implementation pointer is freed in
 *    the destructor automatically.  If the destructor is not declared or is
 *    defined inline in the header file, a compilation error occurs instead
 *    of a memory leak or undefined behavior.
 *  - Exception safety in constructors: the implementation pointer is freed
 *    correctly even if the constructor of the containing class throws after
 *    the implementation class is constructed.
 *  - Copy and/or assignment is automatically disallowed if explicit copy
 *    constructor and/or assignment operator is not provided.
 *  - Compiler helps to manage const-correctness: in const methods, it is not
 *    possible to change the implementation class.
 *
 * Move construction and assignment are also disallowed, but can be enabled by
 * providing explicit move constructor and/or assignment.
 *
 * Intended use:
 * \code
   // In exampleclass.h
   class ExampleClass
   {
       public:
           ExampleClass();
           ~ExampleClass(); // Must not be defined inline

           // <...>

       private:
           class Impl;

           PrivateImplPointer<Impl> impl_;
   };

   // In exampleclass.cpp

   // <definition of ExampleClass::Impl>

   ExampleClass::ExampleClass()
       : impl_(new Impl)
   {
   }

   ExampleClass::~ExampleClass()
   {
   }
   \endcode
 *
 * \inlibraryapi
 * \ingroup module_utility
 */
template <class Impl>
class PrivateImplPointer
{
    public:
        //! Allow implicit initialization from nullptr to support comparison.
        PrivateImplPointer(std::nullptr_t) : ptr_(nullptr) {}
        //! Initialize with the given implementation class.
        explicit PrivateImplPointer(Impl *ptr) : ptr_(ptr) {}
        //! \cond
        // Explicitly declared to work around MSVC problems.
        PrivateImplPointer(PrivateImplPointer &&other) : ptr_(std::move(other.ptr_)) {}
        PrivateImplPointer &operator=(PrivateImplPointer &&other)
        {
            ptr_ = std::move(other.ptr_);
            return *this;
        }
        //! \endcond

        /*! \brief
         * Sets a new implementation class and destructs the previous one.
         *
         * Needed, e.g., to implement lazily initializable or copy-assignable
         * classes.
         */
        void reset(Impl *ptr) { ptr_.reset(ptr); }
        //! Access the raw pointer.
        Impl *get() { return ptr_.get(); }
        //! Access the implementation class as with a raw pointer.
        Impl *operator->() { return ptr_.get(); }
        //! Access the implementation class as with a raw pointer.
        Impl &operator*() { return *ptr_; }
        //! Access the implementation class as with a raw pointer.
        const Impl *operator->() const { return ptr_.get(); }
        //! Access the implementation class as with a raw pointer.
        const Impl &operator*() const { return *ptr_; }

        //! Allows testing whether the implementation is initialized.
        explicit operator bool() const { return ptr_ != nullptr; }

        //! Tests for equality (mainly useful against nullptr).
        bool operator==(const PrivateImplPointer &other) const { return ptr_ == other.ptr_; }
        //! Tests for inequality (mainly useful against nullptr).
        bool operator!=(const PrivateImplPointer &other) const { return ptr_ != other.ptr_; }

    private:
        std::unique_ptr<Impl> ptr_;

        // Copy construction and assignment disabled by the unique_ptr member.
};

} // namespace sci

#endif
