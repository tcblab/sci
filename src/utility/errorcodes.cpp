/*! \internal \file
 * \brief
 * Implements functions in errorcodes.h.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#include "errorcodes.h"

namespace sci
{

namespace
{

/*! \brief
 * Strings corresponding to sci::ErrorCode values.
 *
 * This has to match the enum in errorcodes.h!
 *
 * \ingroup module_utility
 */
const char *const error_names[] =
{
    "No error",
    "Out of memory",
    "File not found",
    "System I/O error",
    "Requested tolerance cannot be achieved",

    "Feature not implemented",
    "Invalid value (bug)",
    "Invalid call (bug)",
    "Internal error (bug)",
    "API error (bug)",
    "Range checking error (possible bug)",
    "Communication (parallel processing) problem",

    "Unknown error",
};

}   // namespace

const char *getErrorCodeString(int errorcode)
{
    if (errorcode < 0 || errorcode >= eeUnknownError)
    {
        errorcode = eeUnknownError;
    }
    return error_names[errorcode];
}

} // namespace sci
