/*! \file
 * \brief
 * Declares SCI_CURRENT_FUNCTION for getting the current function name.
 *
 * The implementation is essentially copied from Boost 1.55 (see reference below).
 *
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_CURRENT_FUNCTION_H
#define SCI_UTILITY_CURRENT_FUNCTION_H

/*! \def SCI_CURRENT_FUNCTION
 * \brief
 * Expands to a string that provides the name of the current function.
 *
 * \ingroup module_utility
 */

//
//  boost/current_function.hpp - BOOST_CURRENT_FUNCTION
//
//  Copyright (c) 2002 Peter Dimov and Multi Media Ltd.
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//
//  http://www.boost.org/libs/utility/current_function.html
//

namespace sci
{

namespace internal
{

//! Helper for defining SCI_CURRENT_FUNCTION.
inline void current_function_helper()
{

#if defined(__GNUC__) || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) || (defined(__ICC) && (__ICC >= 600)) || defined(__ghs__)

# define SCI_CURRENT_FUNCTION __PRETTY_FUNCTION__

#elif defined(__DMC__) && (__DMC__ >= 0x810)

# define SCI_CURRENT_FUNCTION __PRETTY_FUNCTION__

#elif defined(__FUNCSIG__)

# define SCI_CURRENT_FUNCTION __FUNCSIG__

#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))

# define SCI_CURRENT_FUNCTION __FUNCTION__

#elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)

# define SCI_CURRENT_FUNCTION __FUNC__

#elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)

# define SCI_CURRENT_FUNCTION __func__

#else

# define SCI_CURRENT_FUNCTION "(unknown)"

#endif

}

} // namespace internal

} // namespace sci

#endif
