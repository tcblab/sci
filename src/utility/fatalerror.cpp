#include "fatalerror.h"

#include <cstddef>
#include <cstdio>
#include <cstdlib>

#include "errorformat.h"

void sci_exit_on_fatal_error(ExitType exitType, int returnValue)
{
    std::fflush(stdout);
    std::fflush(stderr);

    if (exitType == ExitType_CleanExit)
    {
        std::exit(returnValue);
    }
    // We cannot use std::exit() if other threads may still be executing, since that would cause destructors to be
    // called for global objects that may still be in use elsewhere.
    std::_Exit(returnValue);
}
