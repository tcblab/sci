/*! \internal \file
 * \brief
 * Declares an internal helper function for formatting standard error messages.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_ERRORFORMAT_H
#define SCI_UTILITY_ERRORFORMAT_H

#include <cstdio>

namespace sci
{

/*! \cond internal */
namespace internal
{

/*! \brief
 * Formats a common header for fatal error messages.
 *
 * Does not throw.
 *
 * \ingroup module_utility
 */
void printFatalErrorHeader(FILE *fp, const char *title,
                           const char *func, const char *file, int line);
/*! \brief
 * Formats a line of fatal error message text.
 *
 * Does not throw.
 *
 * \ingroup module_utility
 */
void printFatalErrorMessageLine(FILE *fp, const char *text, int indent);
/*! \brief
 * Formats a common footer for fatal error messages.
 *
 * Does not throw.
 *
 * \ingroup module_utility
 */
void printFatalErrorFooter(FILE *fp);

}   // namespace internal
//! \endcond

} // namespace sci

#endif
