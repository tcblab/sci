/*! \internal \file
 * \brief
 * Implements assertion handlers.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#include "assertions.h"

#include <cstdio>
#include <cstdlib>

#include "utility/fatalerror.h"

#include "errorformat.h"

namespace sci
{

/*! \cond internal */
namespace internal
{

void assertHandler(const char *condition, const char *msg,
                   const char *func, const char *file, int line)
{
    printFatalErrorHeader(stderr, "Assertion failed", func, file, line);
    std::fprintf(stderr, "Condition: %s\n", condition);
    printFatalErrorMessageLine(stderr, msg, 0);
    printFatalErrorFooter(stderr);
    sci_exit_on_fatal_error(ExitType_Abort, 1);
}

}   // namespace internal
//! \endcond

} // namespace sci
