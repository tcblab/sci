#include "current_function.h"

/*! \def SCI_RELEASE_ASSERT
 * \brief
 * Macro for asserts that should also be present in the release version.
 *
 * Regardless of NDEBUG, this macro checks \p condition, and if it is not true,
 * it calls the assert handler.
 *
 * Although this macro currently calls abort() if the assertion fails, it
 * should only be used in a context where it is safe to throw an exception to
 * keep the option open.
 */
#ifdef SCI_DISABLE_ASSERTS
#define SCI_RELEASE_ASSERT(condition, msg)
#else
#define SCI_RELEASE_ASSERT(condition, msg) \
    ((void) ((condition) ? (void)0 : \
             ::sci::internal::assertHandler(#condition, msg, \
                                            SCI_CURRENT_FUNCTION, __FILE__, __LINE__)))
#endif
/*! \def SCI_ASSERT
 * \brief
 * Macro for debug asserts.
 *
 * If NDEBUG is defined, this macro expands to nothing.
 * If it is not defined, it will work exactly like ::SCI_RELEASE_ASSERT.
 *
 * \see ::SCI_RELEASE_ASSERT
 */
#ifdef NDEBUG
#define SCI_ASSERT(condition, msg)
#else
#define SCI_ASSERT(condition, msg) SCI_RELEASE_ASSERT(condition, msg)
#endif

namespace sci
{

namespace internal
{

/*! \brief
 * Called when an assert fails.
 *
 * Should not be called directly, but instead through ::SCI_ASSERT or
 * ::SCI_RELEASE_ASSERT.
 *
 * \ingroup module_utility
 */
// noreturn
void assertHandler(const char *condition, const char *msg,
                   const char *func, const char *file, int line);

}   // namespace internal
}   // namespace sci
