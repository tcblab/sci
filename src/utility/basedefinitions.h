/*! \file
 * \brief
 * Basic types and macros used throughout this library.
 *
 * \inpublicapi
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_BASEDEFINITIONS_H
#define SCI_UTILITY_BASEDEFINITIONS_H

#include <stdint.h>
#ifndef _MSC_VER
#include <inttypes.h>
#endif

/*! \name Fixed-width integer types
 *
 * These types and macros provide the equivalent of 32- and 64-bit integer
 * types from C99 headers `stdint.h` and `inttypes.h`.  These headers are also
 * there in C++11.  The types and macros from here should be used instead of
 * `int32_t` etc.
 *
 * (MSVC 2015 still doesn't support the format strings.)
 */
/*! \{ */
typedef int32_t sci_int32_t;
typedef int64_t sci_int64_t;
typedef uint32_t sci_uint32_t;
typedef uint64_t sci_uint64_t;

#ifdef _MSC_VER
#define SCI_PRId32 "I32d"
#define SCI_SCNd32 "I32d"

#define SCI_PRId64 "I64d"
#define SCI_SCNd64 "I64d"

#define SCI_PRIu32 "I32u"
#define SCI_SCNu32 "I32u"

#define SCI_PRIu64 "I64u"
#define SCI_SCNu64 "I64u"
#else
#define SCI_PRId32 PRId32
#define SCI_SCNd32 SCNd32

#define SCI_PRId64 PRId64
#define SCI_SCNd64 SCNd64

#define SCI_PRIu32 PRIu32
#define SCI_SCNu32 SCNu32

#define SCI_PRIu64 PRIu64
#define SCI_SCNu64 SCNu64
#endif

#define SCI_INT32_MAX INT32_MAX
#define SCI_INT32_MIN INT32_MIN

#define SCI_INT64_MAX INT64_MAX
#define SCI_INT64_MIN INT64_MIN

#define SCI_UINT32_MAX UINT32_MAX
#define SCI_UINT32_MIN UINT32_MIN

#define SCI_UINT64_MAX UINT64_MAX
#define SCI_UINT64_MIN UINT64_MIN
/*! \} */

/*! \def sci_unused
 * \brief
 * Attribute to suppress compiler warnings about unused function parameters.
 *
 * This attribute suppresses compiler warnings about unused function arguments
 * by marking them as possibly unused.  Some arguments are unused but
 * have to be retained to preserve a function signature
 * that must match that of another function.
 * Some arguments are only used in *some* conditional compilation code paths.
 */
#ifndef sci_unused
#ifdef __GNUC__
/* GCC, clang, and some ICC pretending to be GCC */
#  define sci_unused __attribute__ ((unused))
#elif (defined(__INTEL_COMPILER) || defined(__ECC)) && !defined(_MSC_VER)
/* ICC on *nix */
#  define sci_unused __attribute__ ((unused))
#elif defined(__PGI)
/* Portland group compilers */
#  define sci_unused __attribute__ ((unused))
#elif defined _MSC_VER
/* MSVC */
#  define sci_unused /*@unused@*/
#elif defined(__xlC__)
/* IBM */
#  define sci_unused __attribute__ ((unused))
#else
#  define sci_unused
#endif
#endif

#ifndef __has_feature
/** For compatibility with non-clang compilers. */
#define __has_feature(x) 0
#endif

/*! \def sci_noreturn
 * \brief
 * Indicate that a function is not expected to return.
 */
#ifndef sci_noreturn
#if defined(__GNUC__) || __has_feature(attribute_analyzer_noreturn)
#define sci_noreturn __attribute__((noreturn))
#elif defined (_MSC_VER)
#define sci_noreturn __declspec(noreturn)
#else
#define sci_noreturn
#endif
#endif

/*! \def sci_constexpr
 * \brief C++11 constexpr everywhere.
 *
 * Support for constexpr was not added until MSVC 2015, and it still
 * seems to be unreliable. Since interacting with parts of libc++ and
 * libstdc++ depend on it (for instance the min/max calls in our
 * random engines), we need to specify it for other compilers.
 */
#ifndef sci_constexpr
#if !defined(_MSC_VER)
#    define sci_constexpr constexpr
#else
#    define sci_constexpr
#endif
#endif

/*! \def SCI_ALIGNED(type, alignment)
 * \brief
 * Declare variable with data alignment
 *
 * \param[in] type       Type of variable
 * \param[in] alignment  Alignment in multiples of type
 *
 * Typical usage:
 * \code
   SCI_ALIGNED(real, SCI_SIMD_REAL_WIDTH) buf[...];
   \endcode
 */

// This will for instance work for MSVC2015 and later.  If you get an
// error here, find out what attribute to use to get your compiler to
// align data properly and add it as a case.
#define SCI_ALIGNED(type, alignment) alignas(alignment*sizeof(type)) type

/*! \brief
 * Macro to explicitly ignore an unused value.
 *
 * \ingroup module_utility
 */
#define SCI_UNUSED_VALUE(value) (void)value

#endif
