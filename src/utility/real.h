/*! \file
 * \brief
 * Declares `real` and related constants.
 *
 * \inpublicapi
 * \ingroup module_utility
 */
#ifndef SCI_UTILITY_REAL_H
#define SCI_UTILITY_REAL_H

/*! \brief Double precision accuracy */
#define SCI_DOUBLE_EPS   2.2204460492503131e-16

/*! \brief Maximum double precision value - reduced 1 unit in last digit for MSVC */
#define SCI_DOUBLE_MAX   1.7976931348623157e+308

/*! \brief Minimum double precision value */
#define SCI_DOUBLE_MIN   2.2250738585072014e-308

/*! \brief Single precision accuracy */
#define SCI_FLOAT_EPS    1.19209290e-07F

/*! \brief Maximum single precision value - reduced 1 unit in last digit for MSVC */
#define SCI_FLOAT_MAX    3.40282346E+38F

/*! \brief Minimum single precision value */
#define SCI_FLOAT_MIN    1.175494351E-38F

#ifdef __PGI
/* The portland group x86 C/C++ compilers do not treat negative zero initializers
 * correctly, but "optimizes" them to positive zero, so we implement it explicitly.
 * These constructs are optimized to simple loads at compile time. If you want to
 * use them on other compilers those have to support gcc preprocessor extensions.
 * Note: These initializers might be sensitive to the endianness (which can
 * be different for byte and word order), so check that it works for your platform
 * and add a separate section if necessary before adding to the ifdef above.
 */
#    define SCI_DOUBLE_NEGZERO  ({ const union { int  di[2]; double d; } _sci_dzero = {0, -2147483648}; _sci_dzero.d; })
#    define SCI_FLOAT_NEGZERO   ({ const union { int  fi; float f; } _sci_fzero = {-2147483648}; _sci_fzero.f; })
#else
/*! \brief Negative zero in double */
#    define SCI_DOUBLE_NEGZERO  (-0.0)

/*! \brief Negative zero in float */
#    define SCI_FLOAT_NEGZERO   (-0.0f)
#endif

/*! \typedef real
 * \brief Precision-dependent \Gromacs floating-point type.
 */
/*! \def HAVE_REAL
 * \brief Used to check whether `real` is already defined.
 */
/*! \def SCI_MPI_REAL
 * \brief MPI data type for `real`.
 */
/*! \def SCI_REAL_EPS
 * \brief Accuracy for `real`.
 */
/*! \def SCI_REAL_MIN
 * \brief Smallest non-zero value for `real`.
 */
/*! \def SCI_REAL_MAX
 * \brief Largest finite value for `real`.
 */
/*! \def SCI_REAL_NEGZERO
 * \brief Negative zero for `real`.
 */
/*! \def sci_real_fullprecision_pfmt
 * \brief Format string for full `real` precision.
 */
#if SCI_DOUBLE

#ifndef HAVE_REAL
typedef double      real;
#define HAVE_REAL
#endif

#define SCI_MPI_REAL      MPI_DOUBLE
#define SCI_REAL_EPS      SCI_DOUBLE_EPS
#define SCI_REAL_MIN      SCI_DOUBLE_MIN
#define SCI_REAL_MAX      SCI_DOUBLE_MAX
#define SCI_REAL_NEGZERO  SCI_DOUBLE_NEGZERO
#define sci_real_fullprecision_pfmt "%21.14e"

#else /* SCI_DOUBLE */

#ifndef HAVE_REAL
typedef float           real;
#define HAVE_REAL
#endif

#define SCI_MPI_REAL      MPI_FLOAT
#define SCI_REAL_EPS      SCI_FLOAT_EPS
#define SCI_REAL_MIN      SCI_FLOAT_MIN
#define SCI_REAL_MAX      SCI_FLOAT_MAX
#define SCI_REAL_NEGZERO  SCI_FLOAT_NEGZERO
#define sci_real_fullprecision_pfmt "%14.7e"

#endif /* SCI_DOUBLE */

#endif
