/*! \internal \file
 * \brief
 * Implements functions declared in errorformat.h.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#include "errorformat.h"

#include <cctype>
#include <cstdio>
#include <cstring>

//#include "gromacs/utility/basenetwork.h"
//#include "gromacs/utility/baseversion.h"
#include "utility/path.h"
//#include "gromacs/utility/stringutil.h"

namespace sci
{

/*! \cond internal */
namespace internal
{

void printFatalErrorHeader(FILE *fp, const char *title,
                           const char *func, const char *file, int line)
{
    std::fprintf(fp, "\n-------------------------------------------------------\n");
    if (file != nullptr)
    {
        std::fprintf(fp, "Source file: %s (line %d)\n",
                     Path::stripSourcePrefix(file), line);
    }
    if (func != nullptr)
    {
        std::fprintf(fp, "Function:    %s\n", func);
    }
    std::fprintf(fp, "\n");
    std::fprintf(fp, "%s:\n", title);
}

void printFatalErrorMessageLine(FILE *fp, const char *text, int /*indent*/)
{
    // Pretty printing could be done here
    std::fputs(text, fp);
}

void printFatalErrorFooter(FILE *fp)
{
    std::fprintf(fp, "\n-------------------------------------------------------\n");
}

}   // namespace internal
//! \endcond

} // namespace sci
