/*! \internal \file
 * \brief
 * Tests for (some) functions in path.h.
 *
 * \author Teemu Murtola <teemu.murtola@gmail.com>
 * \ingroup module_utility
 */
#include "../path.h"

#include <gtest/gtest.h>

namespace
{

TEST(PathTest, StripSourcePrefixWorks)
{
    EXPECT_STREQ("", sci::Path::stripSourcePrefix(""));
    EXPECT_STREQ("foo.cpp", sci::Path::stripSourcePrefix("foo.cpp"));
    EXPECT_STREQ("foo.cpp", sci::Path::stripSourcePrefix("some/dir/foo.cpp"));
    EXPECT_STREQ("foo.cpp", sci::Path::stripSourcePrefix("src/some/dir/foo.cpp"));
    EXPECT_STREQ("foo.cpp",
                 sci::Path::stripSourcePrefix("srcx/sci/foo.cpp"));
    EXPECT_STREQ("src/sci/foo.cpp",
                 sci::Path::stripSourcePrefix("src/sci/foo.cpp"));
    EXPECT_STREQ("src/sci/foo.cpp",
                 sci::Path::stripSourcePrefix("some/dir/src/sci/foo.cpp"));
    // TODO: For in-source builds, this might not work.
    // TODO somehow does not work for sci build
    /*
    EXPECT_EQ(sci::Path::normalize("src/sci/utility/tests/path.cpp"),
              sci::Path::stripSourcePrefix(__FILE__))
    << "stripSourcePrefix() does not work with compiler-produced file names. "
    << "This only affects source paths reported in fatal error messages.";
    */
}

/*
TEST(PathTest, ConcatenateBeforeExtensionWorks)
{
    EXPECT_STREQ("md1.log", sci::Path::concatenateBeforeExtension("md.log", "1").c_str());
    EXPECT_STREQ("ener0", sci::Path::concatenateBeforeExtension("ener", "0").c_str());
    EXPECT_STREQ("simpledir/traj34.tng", sci::Path::concatenateBeforeExtension("simpledir/traj.tng", "34").c_str());
    EXPECT_STREQ("complex.dir/traj34.tng", sci::Path::concatenateBeforeExtension("complex.dir/traj.tng", "34").c_str());
    EXPECT_STREQ("complex.dir/traj34", sci::Path::concatenateBeforeExtension("complex.dir/traj", "34").c_str());
}
*/

} // namespace
