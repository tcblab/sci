/*! \internal \file
 * \brief
 * Tests for simple math functions.
 *
 * \author Erik Lindahl <erik.lindahl@gmail.com>
 * \ingroup module_math
 */
#include "../functions.h"

#include <cmath>
#include <cstdint>

#include <gtest/gtest.h>

#include "testutils/testasserts.h"

namespace sci
{

namespace
{

TEST(FunctionTest, StaticLog2)
{
    // Trying to test these values explicitly leads to linking errors
    // after the macros expand, somehow.
    auto value = sci::StaticLog2<1>::value;
    EXPECT_EQ(value, 0);
    value = sci::StaticLog2<2>::value;
    EXPECT_EQ(value, 1);
    value = sci::StaticLog2<3>::value;
    EXPECT_EQ(value, 1);
    value = sci::StaticLog2<4>::value;
    EXPECT_EQ(value, 2);
    value = sci::StaticLog2<5>::value;
    EXPECT_EQ(value, 2);
    value = sci::StaticLog2<6>::value;
    EXPECT_EQ(value, 2);
    value = sci::StaticLog2<7>::value;
    EXPECT_EQ(value, 2);
    value = sci::StaticLog2<8>::value;
    EXPECT_EQ(value, 3);
    value = sci::StaticLog2<0xFFFFFFFF>::value;
    EXPECT_EQ(value, 31);
    value = sci::StaticLog2<9876543210>::value; // > 32 bits
    EXPECT_EQ(value, 33);
    value = sci::StaticLog2<0xFFFFFFFFFFFFFFFFULL>::value;
    EXPECT_EQ(value, 63);
}

TEST(FunctionTest, Log2I32Bit)
{
    for (std::uint32_t i = 1; i <= 0xF; i++)
    {
        EXPECT_EQ(sci::log2I(i), static_cast<std::uint32_t>(floor(log2(i))));
    }

    for (std::uint32_t i = 0; i <= 0xF; i++)
    {
        EXPECT_EQ(sci::log2I(static_cast<std::uint32_t>(0xFFFFFFF0 + i)), 31);
    }
}

TEST(FunctionTest, Log2I64Bit)
{
    for (std::uint64_t i = 1; i <= 0xF; i++)
    {
        EXPECT_EQ(sci::log2I(i), static_cast<std::uint64_t>(floor(log2(i))));
    }

    for (std::uint64_t i = 0; i <= 0xF; i++)
    {
        EXPECT_EQ(sci::log2I(static_cast<std::uint64_t>(0xFFFFFFF0ULL+i)), 31);
    }
    for (std::uint64_t i = 0x10; i <= 0x1F; i++)
    {
        EXPECT_EQ(sci::log2I(static_cast<std::uint64_t>(0xFFFFFFF0ULL+i)), 32);
    }

    for (std::uint64_t i = 0; i <= 0xF; i++)
    {
        EXPECT_EQ(sci::log2I(static_cast<std::uint64_t>(0xFFFFFFFFFFFFFFF0ULL+i)), 63);
    }
}

TEST(FunctionTest, GreatestCommonDivisor)
{
    // Trying to test these values explicitly leads to linking errors
    // after the macros expand, somehow.
    auto value = greatestCommonDivisor(24, 64);
    EXPECT_EQ(8, value);
    value = greatestCommonDivisor(64, 24);
    EXPECT_EQ(8, value);
    value = greatestCommonDivisor(169, 289);
    EXPECT_EQ(1, value);
}

TEST(FunctionTest, InvsqrtFloat)
{
    EXPECT_FLOAT_EQ(sci::invsqrt(float(1.0)), 1);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(2.0)), 0.70710677);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(3.0)), 0.57735026);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(4.0)), 0.5);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(5.0)), 0.44721359);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(6.0)), 0.40824828);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(7.0)), 0.3779645);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(8.0)), 0.35355338);
    EXPECT_FLOAT_EQ(sci::invsqrt(float(9.0)), 0.33333334);
}

TEST(FunctionTest, InvsqrtDouble)
{
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(1.0)), 1);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(2.0)), 0.70710678118654746);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(3.0)), 0.57735026918962584);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(4.0)), 0.5);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(5.0)), 0.44721359549995793);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(6.0)), 0.40824829046386307);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(7.0)), 0.3779644730092272);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(8.0)), 0.35355339059327373);
    EXPECT_DOUBLE_EQ(sci::invsqrt(double(9.0)), 0.33333333333333331);
}

TEST(FunctionTest, InvsqrtInteger)
{
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(1)), 1);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(2)), 0.70710678118654724);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(3)), 0.57735026918962551);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(4)), 0.5);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(5)), 0.44721359549995782);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(6)), 0.40824829046386307);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(7)), 0.37796447300922709);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(8)), 0.35355339059327362);
    EXPECT_DOUBLE_EQ(sci::invsqrt(int(9)), 0.33333333333333331);
}

TEST(FunctionTest, InvcbrtFloat)
{
    EXPECT_FLOAT_EQ(sci::invcbrt(float(-5)), -0.58480352);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(-4)), -0.62996054);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(-3)), -0.69336128);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(-2)), -0.79370052);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(-1)), -1);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(1)), 1);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(2)), 0.79370052);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(3)), 0.69336128);
    EXPECT_FLOAT_EQ(sci::invcbrt(float(4)), 0.62996054);
}

TEST(FunctionTest, InvcbrtDouble)
{
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(-5)), -0.58480354764257314);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(-4)), -0.62996052494743648);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(-3)), -0.69336127435063477);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(-2)), -0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(-1)), -1);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(1)), 1);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(2)), 0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(3)), 0.69336127435063477);
    EXPECT_DOUBLE_EQ(sci::invcbrt(double(4)), 0.62996052494743648);
}

TEST(FunctionTest, InvcbrtInteger)
{
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(-5)), -0.58480354764257314);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(-4)), -0.62996052494743648);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(-3)), -0.69336127435063477);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(-2)), -0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(-1)), -1);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(1)), 1);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(2)), 0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(3)), 0.69336127435063477);
    EXPECT_DOUBLE_EQ(sci::invcbrt(int(4)), 0.62996052494743648);
}


TEST(FunctionTest, SixthrootFloat)
{
    EXPECT_FLOAT_EQ(sci::sixthroot(float(0)), 0);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(1)), 1);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(2)), 1.122462);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(3)), 1.2009369);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(4)), 1.2599211);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(5)), 1.3076605);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(6)), 1.3480061);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(7)), 1.3830875);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(8)), 1.4142135);
    EXPECT_FLOAT_EQ(sci::sixthroot(float(9)), 1.4422495);
}

TEST(FunctionTest, SixthrootDouble)
{
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(0)), 0);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(1)), 1);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(2)), 1.122462048309373);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(3)), 1.2009369551760027);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(4)), 1.2599210498948732);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(5)), 1.3076604860118306);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(6)), 1.3480061545972777);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(7)), 1.3830875542684886);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(8)), 1.4142135623730951);
    EXPECT_DOUBLE_EQ(sci::sixthroot(double(9)), 1.4422495703074083);
}

TEST(FunctionTest, SixthrootInteger)
{
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(0)), 0);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(1)), 1);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(2)), 1.122462048309373);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(3)), 1.2009369551760027);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(4)), 1.2599210498948732);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(5)), 1.3076604860118306);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(6)), 1.3480061545972777);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(7)), 1.3830875542684886);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(8)), 1.4142135623730951);
    EXPECT_DOUBLE_EQ(sci::sixthroot(int(9)), 1.4422495703074083);
}

TEST(FunctionTest, InvsixthrootFloat)
{
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(1)), 1);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(2)), 0.8908987);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(3)), 0.83268321);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(4)), 0.79370052);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(5)), 0.76472449);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(6)), 0.74183637);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(7)), 0.72302002);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(8)), 0.70710677);
    EXPECT_FLOAT_EQ(sci::invsixthroot(float(9)), 0.69336128);
}

TEST(FunctionTest, InvsixthrootDouble)
{
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(1)), 1);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(2)), 0.89089871814033927);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(3)), 0.83268317765560429);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(4)), 0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(5)), 0.76472449133173004);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(6)), 0.74183637559040227);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(7)), 0.72302002639948371);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(8)), 0.70710678118654746);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(double(9)), 0.69336127435063477);
}

TEST(FunctionTest, InvsixthrootInteger)
{
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(1)), 1);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(2)), 0.89089871814033927);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(3)), 0.83268317765560429);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(4)), 0.79370052598409968);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(5)), 0.76472449133173004);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(6)), 0.74183637559040227);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(7)), 0.72302002639948371);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(8)), 0.70710678118654746);
    EXPECT_DOUBLE_EQ(sci::invsixthroot(int(9)), 0.69336127435063477);
}

TEST(FunctionTest, Powers)
{
    // These should be remarkably difficult to screw up, but test each
    // of them once anyway with integer and fp arguments to catch typos.
    EXPECT_EQ(4,   sci::square(2));
    EXPECT_EQ(8,   sci::power3(2));
    EXPECT_EQ(16,  sci::power4(2));
    EXPECT_EQ(32,  sci::power5(2));
    EXPECT_EQ(64,  sci::power6(2));
    EXPECT_EQ(4096, sci::power12(2));

    EXPECT_EQ(6.25,               sci::square(2.5));
    EXPECT_EQ(15.625,             sci::power3(2.5));
    EXPECT_EQ(39.0625,            sci::power4(2.5));
    EXPECT_EQ(97.65625,           sci::power5(2.5));
    EXPECT_EQ(244.140625,         sci::power6(2.5));
    EXPECT_EQ(59604.644775390625, sci::power12(2.5));
}

TEST(FunctionTest, ErfInvFloat)
{
    int npoints = 10;
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*0 - npoints + 1) / npoints), -1.163087);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*1 - npoints + 1) / npoints), -0.73286909);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*2 - npoints + 1) / npoints), -0.47693628);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*3 - npoints + 1) / npoints), -0.27246273);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*4 - npoints + 1) / npoints), -0.088855989);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*5 - npoints + 1) / npoints), 0.088855989);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*6 - npoints + 1) / npoints), 0.27246273);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*7 - npoints + 1) / npoints), 0.47693628);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*8 - npoints + 1) / npoints), 0.73286909);
    EXPECT_FLOAT_EQ(sci::erfinv(float(2*9 - npoints + 1) / npoints), 1.163087);
}

TEST(FunctionTest, ErfInvDouble)
{
    int npoints = 10;
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*0 - npoints + 1) / npoints), -1.1630871536766743);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*1 - npoints + 1) / npoints), -0.73286907795921663);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*2 - npoints + 1) / npoints), -0.47693627620446977);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*3 - npoints + 1) / npoints), -0.27246271472675443);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*4 - npoints + 1) / npoints), -0.088855990494257672);
    //EXPECT_DOUBLE_EQ(sci::erfinv(double(2*5 - npoints + 1) / npoints), 0.088855990494257769);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*5 - npoints + 1) / npoints), 0.0888559904942577);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*6 - npoints + 1) / npoints), 0.27246271472675443);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*7 - npoints + 1) / npoints), 0.47693627620446977);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*8 - npoints + 1) / npoints), 0.73286907795921663);
    EXPECT_DOUBLE_EQ(sci::erfinv(double(2*9 - npoints + 1) / npoints), 1.1630871536766734);
}

TEST(FunctionTest, ErfAndErfInvAreInversesFloat)
{
    int npoints = 1000;

    for (int i = 0; i < npoints; i++)
    {
        float r = float(2*i - npoints + 1) / npoints;
        EXPECT_FLOAT_EQ_TOL(r, std::erf(sci::erfinv(r)), sci::test::ulpTolerance(10));
    }
}

TEST(FunctionTest, ErfAndErfInvAreInversesDouble)
{
    int npoints = 1000;

    for (int i = 0; i < npoints; i++)
    {
        double r = double(2*i - npoints + 1) / npoints;
        EXPECT_DOUBLE_EQ_TOL(r, std::erf(sci::erfinv(r)), sci::test::ulpTolerance(10));
    }
}


} // namespace
} // namespace sci
