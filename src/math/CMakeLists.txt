set(MODULE math)
set(SOURCES
    functions.cpp
    )
add_library(${MODULE} OBJECT ${SOURCES})
target_include_directories(${MODULE} PRIVATE
    ${SCIDIR}
    )

sci_install_headers(
    functions.h
     )

set(TEST_EXE ${MODULE}-tests)
set(TEST_OBJLIB ${TEST_EXE}-objlib)

add_subdirectory(tests)

add_executable(${TEST_EXE}
    ${TEST_MAIN}
    $<TARGET_OBJECTS:${MODULE}>
    $<TARGET_OBJECTS:${MODULE}-tests-objlib>
    $<TARGET_OBJECTS:testutils>
    $<TARGET_OBJECTS:utility>
    )
target_link_libraries(${TEST_EXE} PRIVATE
    googletest
    pthread
    )
target_include_directories(${TEST_EXE} PRIVATE
    ${GOOGLETEST_ROOT_DIR}/include
        )
add_test(${MODULE}-tests ${MODULE}-tests)
